<h1 align="center" style="font-size:43px">Basic Project Deploy</h1>

<p align="center">
  <img src="./images/icon (1).png" style="width:120px" class="no-zoom" />
</p>

## 介绍

docker 部署说明，针对docker去部署服务。此处部署说明针对 `basic project`，做`CentOS`服务部署说明。

## 目录

* [前言](/)
* [部署](/docker/install)
  * [安装 docker](/docker/install?id=docker-安装)
  * [编写 docker-compose](/docker/editCompose)
  * [修改 nginx](/docker/editNginx)
* [IDEA 插件](/idea/install)
  * [安装 Cloud Toolkit](/idea/install?id=安装-cloud-toolkit)
  * [IDEA 部署](/idea/deploy)
